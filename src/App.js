import React, { Component } from 'react';
// import logo from './logo.svg';
// import './App.css';
import todos from './data/todos.json';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            link: 'https://www.youtube.com/watch?v=7ojad6QYuqI&t=1155s',
            todos
        };

        this.handleAddTodo = this.handleAddTodo.bind(this);
    }

    handleRemoveTodo(index) {
        this.setState({
            todos: this.state.todos.filter(function (e, i) {
                return i !== index;
            })
        })
    }

    handleAddTodo(todo) {
        this.setState({ todos: [...this.state.todos, todo] })

    }

    render() {
        return (
            <div className="container">
                <div>
                    <a href={this.state.link} target="_blank">tutorial</a>
                </div>
                <hr />
                <TodoInput onAddTodo={this.handleAddTodo}></TodoInput>
                <hr />

                <h4>Todo Count:<span className="badge">{this.state.todos.length}</span>
                </h4>
                <ul className="list-group">
                    {this
                        .state
                        .todos
                        .map((todo, index) => {
                            return (
                                <li className="list-group-item" key={index}>
                                    <h4>{todo.todoTitle}
                                        <small>
                                            <span className="label label-info">{todo.todoPriority}</span>
                                        </small>
                                    </h4>
                                    <p>
                                        <span className="glyphicon glyphicon-user"></span>{todo.todoResponsible}
                                    </p>
                                    <p>{todo.todoDescription}</p>
                                    <p>
                                        <button className="btn btn-danger btn-sm" onClick={this.handleRemoveTodo.bind(this, index)}>
                                            <span className="glyphicon glyphicon-trash"></span>Delete
                                        </button>
                                    </p>
                                </li>
                            )
                        })}
                </ul>
            </div>
        );
    }
}

class TodoInput extends Component {
    constructor(props) {
        super(props);

        this.state = {
            todoTitle: '',
            todoResponsible: '',
            todoDescription: '',
            todoPriority: 'Medium'
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(e) {
        const target = e.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        })
    }

    handleSubmit(e) {
        e.preventDefault();

        this.props.onAddTodo(this.state);
        this.setState({
            todoTitle: '',
            todoDescription: '',
            todoResponsible: '',
            todoPriority: ''
        })
    }

    render() {
        return (
            <div>
                <h4>Add New Todo</h4>
                <form className="form-horizontal" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="inputTodoTitle" className="col-sm-2 control-label">Title</label>
                        <div className="col-sm-10">
                            <input type="text"
                                name="todoTitle"
                                className="form-control"
                                id="inputTodoTitle"
                                value={this.state.todoTitle}
                                onChange={this.handleInputChange}
                                placeholder="Title"
                                required
                            />
                        </div>
                    </div>

                    <div className="form-group">
                        <label htmlFor="inputTodoResponsible" className="col-sm-2 control-label">Responsible</label>
                        <div className="col-sm-10">
                            <input type="text"
                                name="todoResponsible"
                                className="form-control"
                                id="inputTodoResponsible"
                                value={this.state.todoResponsible}
                                onChange={this.handleInputChange}
                                placeholder="Responsible"
                            />
                        </div>
                    </div>

                    <div className="form-group">
                        <label htmlFor="inputTodoDescription" className="col-sm-2 control-label">Description</label>
                        <div className="col-sm-10">
                            <textarea type="text"
                                name="todoDescription"
                                className="form-control"
                                id="inputTodoDescription"
                                value={this.state.todoDescription}
                                onChange={this.handleInputChange}
                                placeholder="Description"
                            />
                        </div>
                    </div>

                    <div className="form-group">
                        <label htmlFor="inputTodoPriority" className="col-sm-2 control-label">Priority</label>
                        <div className="col-sm-10">
                            <select type="text"
                                name="todoPriority"
                                className="form-control"
                                id="inputTodoPriority"
                                value={this.state.todoPriority}
                                onChange={this.handleInputChange}>
                                <option value="Low">Low</option>
                                <option value="Medium">Medium</option>
                                <option value="High">High</option>
                            </select>
                        </div>
                    </div>

                    <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-10">
                            <button type="submit" className="btn btn-primary">Add Todo</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

export default App;